class Employee{
    constructor(name, age, salary) {
        this.name = name;
        this.age = age;
        this.salary = salary;
    }

    get name() {
        return this._name;
    }

    set name(value) {
        this._name = value;
    }

    get age() {
        return this._age;
    }

    set age(value) {
        this._age = value;
    }

    get salary() {
        return this._salary;
    }

    set salary(value) {
        this._salary= value;
    }
}

class Programmer extends Employee{
    constructor(name, age, salary, lang = []){
        super(name, age, salary);
        this.lang = lang;
    }

    set salary(value) {
        this._salary = value * 3;
    }

    get salary() {
        return this._salary;
    }

}


let prog1 = new Programmer('John', 32, 2000, 'Java');
let prog2 = new Programmer('Nick', 48, 5000, ['Java', 'Java Script']);
let prog3 = new Programmer('Rick', 18, 1000, ["C", "C++", "Python"]);


console.log(prog1);
console.log(prog2);
console.log(prog3);
